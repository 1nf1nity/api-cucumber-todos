package stepDefinitions;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;

public class loginSteps 
{
    @Given("^I try to login on the login page$")
    public void navigate_to_login_page() throws UnirestException {}

    
    @When("^I’m using an invalid username and correct password$")
    public void enter_invalid_username_valid_password() throws  UnirestException {}

    @Then("^I cannot login$")
    public void login_page_is_open() {}
}